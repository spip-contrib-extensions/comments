# Changelog

## 4.2.0 - 2025-02-27

### Changed

- Compatible SPIP 4.2 minimum
- Chaînes de langue au format SPIP 4.1+

## 4.1.2 - 2024-09-30

### Fixed

- #12 Pas d'ajax sur la soumission définitive du forum

## 4.1.0 - 2024-07-27

### Changed

- Compatibilité SPIP 4.*

### Fixed

- #11 Ajout d'une chaine de langue de plugins-dist/forum précisant "extensions autorisées" devant les extensions validées

## 4.0.3 - 2023-12-23

### Fixed

- #9 Suppression des balises #DIV remplacées par un simple "div"

## 4.0.2 - 2023-03-08

### Changed

- Comptatible avec SPIP 4.2 et PHP 8.2

### Fixed

- #6 Éviter une notice lors de la prévisualisation d'un article

### Removed

- #3 Supprimer le filtre lignes_longues, maintenant géré en css

## 4.0.1 - 2022-05-12

### Changed

- Comptatible avec SPIP 4.1 et PHP 8.1
