<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/comments.git

return [

	// C
	'comments_description' => 'Affichage des messages en liste, façon commentaires de blog, avec formulaire simplifié. Commentaires microformatés, nomenclature homogène.',
	'comments_slogan' => 'Des commentaires, tout simplement',
];
