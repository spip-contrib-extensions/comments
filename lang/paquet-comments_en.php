<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'Display messages in list, as blog comments, with simplified form. Comments as microformats, uniform nomenclature.',
	'comments_slogan' => 'Comments, simply',
];
