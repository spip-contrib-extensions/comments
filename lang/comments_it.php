<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/comments?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_forum_champs_obligatoires' => 'Campi obbligatori',
	'cfg_forum_format_fil' => 'Elenco o thread?',
	'cfg_forum_longueur' => 'Lunghezza dei messaggi',
	'cfg_forum_longueur_maxi' => 'Lunghezza massima',
	'cfg_forum_longueur_mini' => 'Lunghezza minima',
	'cfg_forum_permalien' => 'Permalink',
	'cfg_label_apparence_permalien' => 'Aspetto del permalink al commento',
	'cfg_label_apparence_permalien_aucun' => 'Nessuno',
	'cfg_label_apparence_permalien_compteur' => 'Contatore (1)',
	'cfg_label_apparence_permalien_diese' => 'Cancelletto (#)',
	'cfg_label_apparence_permalien_picto' => 'Pittogramma',
	'cfg_label_email_obligatoire' => 'Rendi obbligatorio l’inserimento di un’email',
	'cfg_label_nom_obligatoire' => 'Rendi obbligatorio inserire un nome o un nickname',
	'cfg_label_presentation_fil' => 'Panoramica del thread',
	'cfg_label_presentation_fil_liste' => 'Elenco dei commenti (piatto)',
	'cfg_label_presentation_fil_thread' => 'Riga dei commenti (thread)',
	'cfg_label_presentation_fil_thread1' => 'Riga di commenti a un livello',
	'comment' => 'Commento',
	'comments' => 'Commenti',
	'comments_h' => 'Tuoi commenti',

	// D
	'date_heure_a' => 'a',
	'date_jour_le' => 'su',

	// F
	'forum_qui_etes_vous' => 'Chi sei?',

	// L
	'label_email' => 'Email (non sarà pubblicata)',
	'label_nom' => 'Nome',
	'label_notification' => 'Avvisami di eventuali nuovi commenti a questo thread via e-mail',
	'label_url' => 'Tuo sito web',
	'lien_suivre_commentaires' => 'Segui i commenti:',

	// M
	'moderation_info' => 'Attenzione, il tuo messaggio sarà pubblicato solo dopo essere stato controllato ed approvato.',

	// P
	'permalink_to' => 'Link permanente al commento',

	// R
	'reponse_comment_modere' => 'Il tuo commento è stato salvato ed è in attesa di verifica prima di essere pubblicato.',
	'reponse_comment_ok' => 'Grazie per il tuo commento!',

	// S
	'saisie_texte_info' => 'Questo campo accetta scorciatoie SPIP <code>{{gras}}</code> <code>{italique}</code> <code>-*liste</code> <code>[texte-&gt;url]</code> <code>&lt;quote&gt;</code> <code>&lt;code&gt;</code> ed il codice HTML <code>&lt;q&gt;</code> <code>&lt;del&gt;</code> <code>&lt;ins&gt;</code>. Per creare paragrafi lasciare semplicemente delle righe vuote.',
	'saisie_texte_legend' => 'Inserisci qui il tuo commento',
	'submit1' => 'Anteprima',
	'submit2' => 'Invia',

	// T
	'titre_comments' => 'Commenti',
];
