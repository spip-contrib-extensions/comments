<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'Exibição das mensagens em lista, no estilo dos comentáŕios de blog, com formulário simplificado. Comentários microformatados, nomenclatura homogênea.',
	'comments_slogan' => 'Comentários, simplesmente',
];
