<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'ACHTUNG, VERSION IN ENTWICKLUNG FÜR SPIP 3 !<br />Beiträge werden über ein vereinfachtes Formular eingegeben und als Liste im Blog-Stil angezeigt. Kommentare als Microformate, einheitlicher Namensraum.', # MODIF
	'comments_slogan' => 'Vereinfachte Kommentarfunkton',
];
