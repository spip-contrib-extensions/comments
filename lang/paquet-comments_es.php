<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'Visualización de mensajes en lista, como comentarios de blog, con formulario simplificado. Comentarios microformateados, nomenclatura homogénea.',
	'comments_slogan' => 'Comentarios, todo',
];
