<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'POZOR, VERZIA VO VÝVOJI PRE SPIP 3!<br />Zobrazenie príspevkov v zozname, ako na blogu cez zjednodušený formulár. Málo formátované komentáre, jednotný nadpis.', # MODIF
	'comments_slogan' => 'Komentáre celkom jednoducho',
];
