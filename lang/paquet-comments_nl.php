<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'LET OP, VERSIE IN ONWIKKELING, VOOR SPIP 3 !<br />Tonen van een lijst van berichten als blog met een vereenvoudigd formulier. Commentaar in microformaat, met een homogene naamgeving.', # MODIF
	'comments_slogan' => 'Commentaren en niets meer',
];
