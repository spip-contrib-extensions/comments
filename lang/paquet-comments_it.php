<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'Visualizzazione dei messaggi in un elenco, come i commenti del blog, con un modulo semplificato. Commenti microformattati, nomenclatura coerente.',
	'comments_slogan' => 'I commenti, semplicemente',
];
