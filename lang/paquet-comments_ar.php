<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// C
	'comments_description' => 'تنبيه، نسخة قيد التطوير!<br />عرض الاستراكات على طريقة تعليقات المدونات مع استمارة مبسطة.
التعليقات تظهر بالتنسيق المصغّر والتسميات متناسقة.', # MODIF
	'comments_slogan' => 'ببساطة، تعليقات',
];
