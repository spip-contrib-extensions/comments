<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/comments?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_forum_champs_obligatoires' => 'Обов’язкові поля',
	'cfg_forum_format_fil' => 'Списком чи тредом?',
	'cfg_forum_longueur' => 'Довжина повідомлення',
	'cfg_forum_longueur_maxi' => 'Максимальна довжина',
	'cfg_forum_longueur_mini' => 'Мінімальна довжина',
	'cfg_forum_permalien' => 'Постійне посилання',
	'cfg_label_apparence_permalien' => 'Зовнішній вигляд постійного посилання на коментар',
	'cfg_label_apparence_permalien_aucun' => 'Нічого',
	'cfg_label_apparence_permalien_compteur' => 'Лічильник (1.)',
	'cfg_label_apparence_permalien_diese' => 'Решітка (#)',
	'cfg_label_apparence_permalien_picto' => 'Піктограма',
	'cfg_label_email_obligatoire' => 'Зробіть обов’язковим вказання електронної пошти',
	'cfg_label_nom_obligatoire' => 'Обов’язкове введення імені або псевдоніма ',
	'cfg_label_presentation_fil' => 'Тредом',
	'cfg_label_presentation_fil_liste' => 'Списком (в один рівень)',
	'cfg_label_presentation_fil_thread' => 'Коментарі відображаються тредом',
	'cfg_label_presentation_fil_thread1' => 'Коментарі тредом (1 рівень)',
	'comment' => 'коментар',
	'comments' => 'коментарі',
	'comments_h' => 'Ваші коментарі',

	// D
	'date_heure_a' => 'о',
	'date_jour_le' => '',

	// F
	'forum_qui_etes_vous' => 'Ваше ім’я',

	// L
	'label_email' => 'Email (не буде відображатися)',
	'label_nom' => 'Ім’я',
	'label_notification' => 'Повідомляти на мій ємейл про нові коментарі в цьому обговоренні',
	'label_url' => 'Ваш веб сайт',
	'lien_suivre_commentaires' => 'Слідкувати за обговоренням:',

	// M
	'moderation_info' => 'Зверніть увагу, що ваше повідомлення з’явиться тільки після перевірки модератором',

	// P
	'permalink_to' => 'Постійне посилання на коментар',

	// R
	'reponse_comment_modere' => 'Ваш коментар було збережено, він очкує на перевірку перед публікацією.',
	'reponse_comment_ok' => 'Дякуємо за ваш коментар !',

	// S
	'saisie_texte_info' => 'Використовуйте SPIP розмітку<code>{{жирний}}</code> <code>{курсив}</code> <code>-*список</code> <code>[текст-&gt;урл]</code> <code>&lt;цитата&gt;</code> <code>&lt;код&gt;</code> та HTML теги<code>&lt;q&gt;</code> <code>&lt;del&gt;</code> <code>&lt;ins&gt;</code>. Щоб створити абзаци, просто залиште порожні рядки',
	'saisie_texte_legend' => 'Напишіть ваш коментар',
	'submit1' => 'Попередній перегляд',
	'submit2' => 'Надіслати',

	// T
	'titre_comments' => 'Коментарі',
];
